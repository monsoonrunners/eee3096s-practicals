import busio
import digitalio
# note: importing digitalio automatically executes RPi.GPIO.setmode(RPi.GPIO.BCM)
# so in order to use GPIO directly via RPi.GPIO BCM numbering is used
# RPi.GPIO is used directly because digitalio doesn't support interrupts
# so polling would have to be used otherwise
import board
import adafruit_mcp3xxx.mcp3008 as MCP
from adafruit_mcp3xxx.analog_in import AnalogIn
import threading
import RPi.GPIO as gpio
from time import time

spi = cs = mcp = ldr_chan = mcp9700_chan = None

runtime = 0
start_time = 0

mcp9700_dv_dtemp = 0.01
update_periods = [10, 5, 1]
current_sample_period = 0
btn_pin = 26 #BCM pin 26 = Board pin 37


def setup():
    global spi, cs, mcp, ldr_chan, mcp9700_chan

    gpio.setup(btn_pin, gpio.IN, pull_up_down=gpio.PUD_UP)
    gpio.add_event_detect(btn_pin, gpio.FALLING, callback=btn_press, bouncetime=200)

    spi = busio.SPI(clock = board.SCK, MISO = board.MISO, MOSI = board.MOSI)
    cs = digitalio.DigitalInOut(board.D5)
    mcp = MCP.MCP3008(spi, cs)

    ldr_chan = AnalogIn(mcp, MCP.P2)
    mcp9700_chan = AnalogIn(mcp, MCP.P1)


def btn_press(channel):
    global current_sample_period

    if current_sample_period < 2:
        current_sample_period += 1
    else:
        current_sample_period = 0

    print(f'Update period changed to {update_periods[current_sample_period]}s')
    

def adc_read():
    global runtime

    thread = threading.Timer(update_periods[current_sample_period], adc_read)
    thread.daemon = True
    thread.start()

    runtime = time() - start_time

    ldr_val = ldr_chan.value
    ldr_voltage = ldr_chan.voltage

    mcp9700_val = mcp9700_chan.value
    mcp9700_voltage = mcp9700_chan.voltage
    mcp9700_temp = (1/mcp9700_dv_dtemp * mcp9700_voltage) - 50

    adc_print(runtime, mcp9700_val, mcp9700_temp, ldr_val)

def adc_print(*args):
    str_args = []
    for arg in args:
        if type(arg) == type(0.1):
            temp = '{:4.2f}'.format(arg)
        else:
            temp = str(arg)

        str_args.append(temp.ljust(25))

    print('{} {} {} {}'.format(*str_args))


def main():
    global start_time

    print(f'{"Runtime".ljust(25)} {"Temp Reading".ljust(25)} {"Temp".ljust(25)} {"Light Reading".ljust(25)}')

    start_time = time()

    adc_read()

    while True:
        pass


if __name__ == '__main__':
    try:
        setup()
        main()
    except Exception as e:
        print(e)
    finally:
        gpio.cleanup()
