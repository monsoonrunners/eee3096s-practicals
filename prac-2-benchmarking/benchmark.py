#!/usr/bin/python
import subprocess
import re
from global_vars import *


makefile = open(MAKEFILE_PATH, 'r')
makefile_content = makefile.read()
makefile.close()

default_make_flags = makefile_content.split('\n')[2]
print(default_make_flags)

def change_makefile_flags(flags):
    temp = makefile_content.split('\n')
    temp[2] = default_make_flags + ' ' + ' '.join(flags)
    temp = list(map(lambda item: item + '\n', temp))

    makefile = open(MAKEFILE_PATH, 'w')
    makefile.writelines(temp)
    makefile.close()

def execute_and_write_results(run_cmd, compile_cmd=False, cwd='./', iters=5, desc=''):
    if desc:
        print('Running: ' + desc)
        results.write(desc + 50*'#' + '\n')
    else:
        print('Running: ' + ' '.join(run_cmd) + 50*'#')
        results.write(' '.join(run_cmd) + 50*'#' + '\n')

    if compile_cmd:
        subprocess.run(compile_cmd, cwd=cwd)

    for i in range(iters):
        result = subprocess.run(run_cmd, capture_output=True, text=True).stdout
        results.write(result + "\n")


def run_unthreaded_c_benchmark():
    results.write('---- Unthreaded C ' + 100*'-' + '\n\n')

    compile_cmd = ['make']

    for flag in C_OPT_FLAGS:
        change_makefile_flags([flag])
        execute_and_write_results(stock_c_cmd, compile_cmd, './C/', iters, desc = ' '.join(stock_c_cmd) + ' compiled with flag ' + flag)

def run_threaded_c_benchmark():
    results.write('Threaded C' + 100*'-' + '\n\n')

    compile_cmd = ['make', 'threaded']

    for flag in C_OPT_FLAGS:
        change_makefile_flags([flag])
        execute_and_write_results(threaded_c_cmd, compile_cmd, './C/', iters, desc = ' '.join(threaded_c_cmd) + ' compiled with flag ' + flag)

def run_python_benchmark():
    results.write('Python Results:\n')

    execute_and_write_results(py_cmd, iters = iters)


results = open('./results.txt', "w")



try:
    stock_c_heterodyning = open(STOCK_HETERODYNING_SRC, 'r')
    stock_c_heterodyning_src = stock_c_heterodyning.read()
    stock_c_heterodyning.close()

    threaded_c_heterodyning = open(THREADED_HETERODYNING_SRC, 'r')
    threaded_c_heterodyning_src = threaded_c_heterodyning.read()
    threaded_c_heterodyning.close()

    threaded_c_heterodyning_header = open(THREADED_HETERODYNING_HEADER, 'r')
    threaded_c_heterodyning_header_content = threaded_c_heterodyning_header.read()
    threaded_c_heterodyning_header.close()

    c_globals = open(C_GLOBALS_PATH, 'r')
    c_globals_data = c_globals.read();
    c_globals.close()



    results.write('Using float' + 100*'=' + '\n\n')

    run_unthreaded_c_benchmark()


    results.write('Thread-count testing' + 100*'=' + '\n\n')
    for thread_count in [2, 4, 8, 16, 32]:
        threaded_c_heterodyning_header = open(THREADED_HETERODYNING_HEADER, 'w')
        threaded_c_heterodyning_header.write(re.sub(r'\bThread_Count\b\s[0-9]', 'Thread_Count ' + str(thread_count), threaded_c_heterodyning_header_content))
        threaded_c_heterodyning_header.close()

        results.write('Thread Count: ' + str(thread_count))
        run_threaded_c_benchmark()


    results.write('Using double' + 100*'=' + '\n\n')

    stock_c_heterodyning = open(STOCK_HETERODYNING_SRC, 'w')
    stock_c_heterodyning.write(re.sub(r'\bfloat\b', 'double', stock_c_heterodyning_src))
    stock_c_heterodyning.close()

    threaded_c_heterodyning = open(THREADED_HETERODYNING_SRC, 'w')
    threaded_c_heterodyning.write(re.sub(r'\bfloat\b', 'double', threaded_c_heterodyning_src))
    threaded_c_heterodyning.close()

    c_globals = open(C_GLOBALS_PATH, 'w');
    c_globals.write(re.sub(r'\bfloat\b', 'double', c_globals_data))
    c_globals.close()

    run_unthreaded_c_benchmark()


    results.write('Using __fp16' + 100*'=' + '\n\n')

    stock_c_heterodyning = open(STOCK_HETERODYNING_SRC, 'w')
    stock_c_heterodyning.write(re.sub(r'\bfloat\b', '__fp16', stock_c_heterodyning_src))
    stock_c_heterodyning.close()

    threaded_c_heterodyning = open(THREADED_HETERODYNING_SRC, 'w')
    threaded_c_heterodyning.write(re.sub(r'\bfloat\b', '__fp16', threaded_c_heterodyning_src))
    threaded_c_heterodyning.close()

    c_globals = open(C_GLOBALS_PATH, 'w');
    c_globals.write(re.sub(r'\bfloat\b', '__fp16', c_globals_data))
    c_globals.close()

    C_OPT_FLAGS = list(map(lambda item: item + ' -mfp16-format=ieee', C_OPT_FLAGS))

    run_unthreaded_c_benchmark()

    results.close()

except:
    results.close()

