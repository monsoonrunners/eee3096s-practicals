#!/usr/bin/python
MAKEFILE_PATH = './C/makefile'

STOCK_HETERODYNING_SRC = './C/src/CHeterodyning.c'
STOCK_HETERODYNING_HEADER = './C/src/CHeterodyning.h'
STOCK_C_EXECUTABLE = './C/bin/CHeterodyning'

THREADED_HETERODYNING_SRC = './C/src/CHeterodyning_threaded.c'
THREADED_HETERODYNING_HEADER = './C/src/CHeterodyning_threaded.h'
THREADED_C_EXECUTABLE = './C/bin/CHeterodyning_threaded'

C_GLOBALS_PATH = './C/src/globals.h'

PY_EXECUTABLE = './Python/PythonHeterodyning.py'
C_OPT_FLAGS = ['-O0', '-O1', '-O2', '-O3', '-Ofast', '-Os', '-Og', '-funroll-loops', '-O0 -funroll-loops', '-O1 -funroll-loops', '-O2 -funroll-loops', '-O3 -funroll-loops', '-Ofast -funroll-loops', '-Os -funroll-loops', '-Og -funroll-loops']

py_cmd = ['python3', PY_EXECUTABLE]
stock_c_cmd = [STOCK_C_EXECUTABLE]
threaded_c_cmd = [THREADED_C_EXECUTABLE]

iters = 8
