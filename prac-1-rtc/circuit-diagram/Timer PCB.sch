EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 6600 3900 0    50   Input ~ 0
+
Text HLabel 6550 4150 0    50   Input ~ 0
-
Text HLabel 7200 4100 0    50   Input ~ 0
NC
Text HLabel 7700 4000 0    50   Input ~ 0
D
Text HLabel 7400 4400 0    50   Input ~ 0
C
$EndSCHEMATC
