#ifndef BINCLOCK_H
#define BINCLOCK_H

int hFormat(int hours);
void lightHours(int units);
void lightMins(int units);
int hexCompensation(int units);
int decCompensation(int units);
void initGPIO(void);
void secPWM(int units);
void hourInc(void);
void minInc(void);
void toggleTime(void);

const char RTCAddr = 0x68; 
const char SEC_REGISTER = 0x00;
const char MIN_REGISTER = 0x01;
const char HOUR_REGISTER = 0x02;
const char TIMEZONE = 2; // +02H00 (RSA)

const int LED = 0;
const int SECS = 1;
const int BTNS[] = {4, 5};

#endif
