/*
 * BinClock.c
 * Jarrod Olivier
 * Modified by Keegan Crankshaw
 * Further Modified By: Mark Njoroge 
 *
 * 
 * <VLHLEF001> <FSTGAV002>
 * 16/08/2021
*/

#include <signal.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <stdio.h>
#include <stdlib.h>

#include "BinClock.h"
#include "CurrentTime.h"


int hours, mins, secs;
long lastInterruptTime = 0; //Used for button debounce
int RTC;

int HH,MM,SS;

void CleanUp(int sig){
    printf("Cleaning up\n");

    digitalWrite(LED, 0);
    pinMode(LED, INPUT);

    for (int j=0; j < sizeof(BTNS)/sizeof(BTNS[0]); j++) {
        pinMode(BTNS[j],INPUT);
    }

    exit(0);

}

void initGPIO(void){
    /* 
     * Sets GPIO using wiringPi pins. see pinout.xyz for specific wiringPi pins
     * You can also use "gpio readall" in the command line to get the pins
     * Note: wiringPi does not use GPIO or board pin numbers (unless specifically set to that mode)
     */
    printf("Setting up\n");
    wiringPiSetup();


    RTC = wiringPiI2CSetup(RTCAddr);
    
    pinMode(LED, OUTPUT);
    digitalWrite(LED, 0);

    printf("LED and RTC done\n");
    
    for(int j=0; j < sizeof(BTNS)/sizeof(BTNS[0]); j++){
        pinMode(BTNS[j], INPUT);
        pullUpDnControl(BTNS[j], PUD_UP);
    }

    wiringPiISR(BTNS[0], INT_EDGE_RISING, (*hourInc));
    wiringPiISR(BTNS[1], INT_EDGE_RISING, (*minInc));

    printf("BTNS done\n");
    printf("Setup done\n");
}


int main(void){
    signal(SIGINT,CleanUp);
    initGPIO();

    for (;;){
        secs = wiringPiI2CReadReg8(RTC, SEC_REGISTER);
        mins = wiringPiI2CReadReg8(RTC, MIN_REGISTER);
        hours = wiringPiI2CReadReg8(RTC, HOUR_REGISTER);

        digitalWrite(LED, !(digitalRead(LED)));
        
        printf("The current time is: %02d:%02d:%02d\n", hexCompensation(hours), hexCompensation(mins), hexCompensation(secs));

        delay(1000);
    }
    return 0;
}

/*
 * Changes the hour format to 12 hours
 */
int hFormat(int hours){
    /*formats to 12h*/
    if (hours >= 24){
        hours = 0;
    }
    else if (hours > 12){
        hours -= 12;
    }
    return (int)hours;
}


/*
 * hexCompensation
 * This function may not be necessary if you use bit-shifting rather than decimal checking for writing out time values
 * Convert HEX or BCD value to DEC where 0x45 == 0d45   
 */
int hexCompensation(int units){

    int unitsU = units%0x10;

    if (units >= 0x50){
        units = 50 + unitsU;
    }
    else if (units >= 0x40){
        units = 40 + unitsU;
    }
    else if (units >= 0x30){
        units = 30 + unitsU;
    }
    else if (units >= 0x20){
        units = 20 + unitsU;
    }
    else if (units >= 0x10){
        units = 10 + unitsU;
    }
    return units;
}


/*
 * decCompensation
 * This function "undoes" hexCompensation in order to write the correct base 16 value through I2C
 */
int decCompensation(int units){
    int unitsU = units%10;

    if (units >= 50){
        units = 0x50 + unitsU;
    }
    else if (units >= 40){
        units = 0x40 + unitsU;
    }
    else if (units >= 30){
        units = 0x30 + unitsU;
    }
    else if (units >= 20){
        units = 0x20 + unitsU;
    }
    else if (units >= 10){
        units = 0x10 + unitsU;
    }
    return units;
}


/*
 * hourInc
 * Fetch the hour value off the RTC, increase it by 1, and write back
 * Be sure to cater for there only being 23 hours in a day
 * Software Debouncing should be used
 */
void hourInc(void){
    //Debounce
    long interruptTime = millis();

    if (interruptTime - lastInterruptTime>200){
        printf("Interrupt 1 triggered, %x\n", hours);

        hours = wiringPiI2CReadReg8(RTC, HOUR_REGISTER);

        if (hours >= decCompensation( 23 )) {
            hours = 0x00;
        } else {
            hours = decCompensation( hexCompensation(hours) + 1 );
        }

        wiringPiI2CWriteReg8(RTC, HOUR_REGISTER, hours);
    }
    lastInterruptTime = interruptTime;
}

/* 
 * minInc
 * Fetch the minute value off the RTC, increase it by 1, and write back
 * Be sure to cater for there only being 60 minutes in an hour
 * Software Debouncing should be used
 */
void minInc(void){
    long interruptTime = millis();

    if (interruptTime - lastInterruptTime>200){
        printf("Interrupt 2 triggered, %x\n", mins);

        mins = wiringPiI2CReadReg8(RTC, MIN_REGISTER);

        if (mins >= decCompensation( 59 )) {
            mins = 0x00;
            hourInc();
        } else {
            mins = decCompensation( hexCompensation(mins) + 1 );
        }

        wiringPiI2CWriteReg8(RTC, MIN_REGISTER, mins);
    }
    lastInterruptTime = interruptTime;
}

void toggleTime(void){
    long interruptTime = millis();

    if (interruptTime - lastInterruptTime>200){
        HH = getHours();
        printf("HH = %d", HH);
        MM = getMins();
        printf("HH = %d", MM);
        SS = getSecs();
        printf("HH = %d", SS);

        /** HH = hFormat(HH); */
        HH = decCompensation(HH);
        wiringPiI2CWriteReg8(RTC, HOUR_REGISTER, HH);

        MM = decCompensation(MM);
        wiringPiI2CWriteReg8(RTC, MIN_REGISTER, MM);

        SS = decCompensation(SS);
        wiringPiI2CWriteReg8(RTC, SEC_REGISTER, 0b10000000+SS);

    }
    lastInterruptTime = interruptTime;
}
