module test();
  reg clk;
  reg [7:0] A, B;
  reg [3:0] opcode;
  wire [7:0] out;
  
  reg [4:0] i;
  
  ALU ut(
    .clk(clk),
    .A(A),
    .B(B),
    .opcode(opcode),
    .out(out));
  
  initial begin
    clk = 0;
    A = 8'b00000011;
    B = 8'b00000010;
    
    $dumpfile("wavedump.vcd");
    $dumpvars;
    
    $display("A        B        out");
    $monitor("%b %b %b", A, B, out);
    
//     The commented code below is the limited testbench for the 4 
//     functions required to be tested in the practical report.
    
//     opcode = 4'b0000;
//       repeat(2) begin
//        	#1 clk = ~clk;
//       end
    
//     opcode = 4'b0110;
//       repeat(2) begin
//        	#1 clk = ~clk;
//       end
    
//     opcode = 4'b1000;
//       repeat(2) begin
//        	#1 clk = ~clk;
//       end
    
//     opcode = 4'b1111;
//       repeat(2) begin
//        	#1 clk = ~clk;
//       end
    
    for(i = 4'b0; i < 5'b10000; i = i + 1'b1)
      begin
        opcode = i;
        repeat(2) begin
          #1 clk = ~clk;
        end
    end
  end
  
endmodule