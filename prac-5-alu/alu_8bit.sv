module ALU(
  input clk,
  input [7:0] A, B,
  input [3:0] opcode,
  output reg [7:0] out);
  
  reg [7:0] acc;
  reg [7:0] tmp;
  
  always @ (posedge clk)
    begin
      case(opcode)
        4'b0000: acc = A + B;
        4'b0001: acc = A - B;
        4'b0010: acc = A * B;
        4'b0011: acc = A / B;
        4'b0100: acc = acc + A;
        4'b0101: acc = acc * A;
        4'b0110: acc = acc + (A * B);
        4'b0111: begin
          tmp = A[7];
          acc = (A << 1'b1);
          acc = acc + tmp;
        end
        4'b1000: begin
          tmp = A[0];
          tmp = tmp << 4'd7;
          acc = (A >> 1'b1);
          acc = acc + tmp;
        end
        4'b1001: acc = A & B;
        4'b1010: acc = A | B;
        4'b1011: acc = A ^ B;
        4'b1100: acc = ~(A & B);
        4'b1101: acc = A == B ? 8'hff : 1'b0;
        4'b1110: acc = A > B ? 8'hff : 1'b0;
        4'b1111: acc = A < B ? 8'hff : 1'b0;
        default: acc = A;
      endcase
      
      out <= acc;
    end
endmodule
