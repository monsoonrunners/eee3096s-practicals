#!/usr/bin/python

import RPi.GPIO as GPIO
import random
import ES2EEPROMUtils
import os
from time import time
from itertools import chain
from re import match
import sys

end_of_game = None
current_guess = 0
guess_count = 0
value = 0
buzzer_freqs = [4, 2, 1]

LED_value_pins = [11, 13, 15]
LED_accuracy_pin = 32
btn_submit = 16
btn_increase = 18
buzzer_pin = 33
eeprom_vcc = 37
eeprom = ES2EEPROMUtils.ES2EEPROM()


def welcome():
    os.system('clear')
    print("  _   _                 _                  _____ _            __  __ _")
    print(r"| \ | |               | |                / ____| |          / _|/ _| |")
    print(r"|  \| |_   _ _ __ ___ | |__   ___ _ __  | (___ | |__  _   _| |_| |_| | ___ ")
    print(r"| . ` | | | | '_ ` _ \| '_ \ / _ \ '__|  \___ \| '_ \| | | |  _|  _| |/ _ \\")
    print(r"| |\  | |_| | | | | | | |_) |  __/ |     ____) | | | | |_| | | | | | |  __/")
    print(r"|_| \_|\__,_|_| |_| |_|_.__/ \___|_|    |_____/|_| |_|\__,_|_| |_| |_|\___|")
    print("")
    print("Guess the number and immortalise your name in the High Score Hall of Fame!")


def menu():
    global end_of_game
    global value

    option = input(
            "Select an option:   H - View High Scores     P - Play Game       Q - Quit\n")
    option = option.upper()

    if option == "H":
        os.system('clear')
        print("HIGH SCORES!!")
        s_count, ss = fetch_scores()
        display_scores(s_count, ss)

        #  return_to_menu = input('Would you like to return to the menu? [y/n]')
        #
        #  if match('[Yy][A-Za-z]*', return_to_menu):
        #      print('Matched y')
        #      welcome()
        #  else:
        #      exit()

    elif option == "P":
        os.system('clear')

        print("Starting a new round!")
        print("Use the buttons on the Pi to make and submit your guess!")
        print("Press and hold the guess button to cancel your game")

        LED_accuracy.start(0)
        buzzer.start(0)

        value = generate_number()
        print(value) # TODO: remove this

        while not end_of_game:
            pass

    elif option == "Q":
        print("Come back soon!")
        exit()
    else:
        print("Invalid option. Please select a valid one!")


def display_scores(count, raw_data):
    print("There are {} scores. Here are the top 3!".format(count))
    for i, d in enumerate(raw_data[:3]):
        print("{} - {} took {} guesses".format(i, d[0], d[1]) )


# Setup Pins
def setup():
    global LED_accuracy
    global buzzer
    GPIO.setmode(GPIO.BOARD)
    
    GPIO.setup(eeprom_vcc, GPIO.OUT)
    GPIO.output(eeprom_vcc, GPIO.HIGH)

    GPIO.setup([btn_submit, btn_increase], GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup([*LED_value_pins, LED_accuracy_pin, buzzer_pin], GPIO.OUT)

    LED_accuracy = GPIO.PWM(LED_accuracy_pin, 50)
    LED_accuracy.start(0)
    
    buzzer = GPIO.PWM(buzzer_pin, buzzer_freqs[2])
    buzzer.start(0)

    GPIO.add_event_detect(btn_increase, GPIO.FALLING, callback=btn_increase_pressed,
            bouncetime=200)
    GPIO.add_event_detect(btn_submit, GPIO.FALLING, callback=btn_guess_pressed,
            bouncetime=200)


def fetch_scores():
    score_count = eeprom.read_byte(0)
    scores = eeprom.read_block(1, 4 * score_count)
    score_names = []
    score_vals = scores[3::4]
    for i in range(len(scores)):
        if ((i + 1)%4):
            score_names.append(scores[i])

    score_names = list(map(lambda x: chr(x), score_names))
    score_names = chunk_list(score_names, 3)
    for i, elmt in enumerate(score_names):
        score_names[i] = ''.join(elmt)

    scores = []
    for i in range(len(score_names)):
        scores.append((score_names[i], score_vals[i]))
    
    return score_count, scores


def chunk_list(lst, chunk_size):
    chunks = []
    for i in range(0, len(lst), chunk_size):
        temp = lst[i:i+chunk_size]
        chunks.append(temp)
    return chunks


def save_scores(name, score):
    s_count = eeprom.read_byte(0)
    ss = eeprom.read_block(1, 4 * s_count)

    s_count += 1

    ss = list(chain(ss, list(map(lambda d: ord(d), list(name))), [score]))

    chunks = chunk_list(ss, 4)

    chunks.sort(key = lambda d: d[3])

    eeprom.write_byte(0, s_count)

    for i, chunk in enumerate(chunks):
        eeprom.write_block(i + 1, chunk)


def generate_number():
    return random.randint(0, pow(2, 3)-1)


def btn_increase_pressed(channel):
    global current_guess
    if current_guess < 7:
        current_guess += 1
    else: 
        current_guess = 0

    GPIO.output((*LED_value_pins,), (*dec_to_bin_arr(current_guess),))


def dec_to_bin_arr(num):
    temp = list(map(lambda x: int(x), bin(num)[2:]))
    while len(temp) < 3:
        temp.insert(0, 0)
    return temp


def btn_guess_pressed(channel):
    global end_of_game
    global guess_count

    hold_time = 0
    interrupt_time = time()

    while not GPIO.input(btn_submit):
        hold_time = time() - interrupt_time

    if hold_time >= 1:
        reset_outputs()
        welcome()
        return menu()

    if current_guess == value:
        reset_outputs()
        name = ''
        print('Congratulations! You win!')
        while not match('^[a-zA-Z]{3}$', name):
            name = input('Please enter a 3 letter name for your score:\n').strip()
            name = name.upper()
            print(name)

        save_scores(name, guess_count)
        print('Score saved!')
        guess_count = 0
        end_of_game = True

    guess_count += 1
    update_accuracy_led()
    trigger_buzzer()


def update_accuracy_led():
    global current_guess
    global value

    if current_guess > value or value == 0:
        dc = (8-current_guess)/(8-value) * 100
    else:
        dc = current_guess/value * 100
    
    LED_accuracy.ChangeDutyCycle(dc)


def trigger_buzzer():
    diff = abs(value - current_guess)
    buzzer.ChangeDutyCycle(0)

    if diff in [1, 2, 3]:
        buzzer.ChangeDutyCycle(50)
        buzzer.ChangeFrequency(buzzer_freqs[diff - 1])


def reset_outputs():
    GPIO.output(LED_value_pins, GPIO.LOW)
    LED_accuracy.ChangeDutyCycle(0)
    LED_accuracy.stop()
    buzzer.ChangeFrequency(buzzer_freqs[2])
    buzzer.stop()


if __name__ == "__main__":
    try:
        setup()
        welcome()
        while True:
            menu()
            pass
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
    finally:
        GPIO.cleanup()
